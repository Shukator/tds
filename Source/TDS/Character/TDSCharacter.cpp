// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"

ATDSCharacter::ATDSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprints/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATDSCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
		MovementTick(DeltaSeconds);
	}
}

void ATDSCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);

	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ATDSCharacter::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ATDSCharacter::InputAxisY);

	//NewInputComponent->BindAction(TEXT("SpawnMagicProjectile"), this, &ATDSCharacter::SpawnMagicProjectile);
}

void ATDSCharacter::BeginPlay()
{
	Super::BeginPlay();

	
}

void ATDSCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ATDSCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ATDSCharacter::MovementTick(float DeltaTime)
{
	FVector FVec = CameraBoom->GetForwardVector();
	FVector RVec = CameraBoom->GetRightVector();
	FRotator Rot = FVec.Rotation();
	Rot.Pitch = 0.0f;
	FVec = Rot.Vector();

	AddMovementInput(FVec, AxisX);
	AddMovementInput(RVec, AxisY);

	APlayerController* MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0); 
	if (MyController)
	{
		FHitResult ResultHit;
		MyController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);

		float FindRotatorResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
		SetActorRotation(FQuat(FRotator(0.0f, FindRotatorResultYaw, 0.0f)));
	}
}

void ATDSCharacter::CharacterUpdate()
{
	float ResSpeed = 600.0f;
	switch (MovementState)
	{
	case EMovementState::Walk_state:
		ResSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::CastWalk_state:
		ResSpeed = MovementInfo.CastWalkSpeed;
		break;
	case EMovementState::Run_state:
		ResSpeed = MovementInfo.RunSpeed;
		break;
	case EMovementState::CastRun_state:
		ResSpeed = MovementInfo.CastRunSpeed;
		break;
	case EMovementState::Jump_state:
		ResSpeed = MovementInfo.JumpSpeed;
		break;
	default:
		break;
	}
	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATDSCharacter::ChangeMovementState()
{
	if (JumpEnabled)
	{
		MovementState = EMovementState::Jump_state;
	}
	else
	{
		if (!RunEnabled && !CastEnabled)
		{
			MovementState = EMovementState::Walk_state;
		}
		if (!RunEnabled && CastEnabled)
		{
			MovementState = EMovementState::CastWalk_state;
		}
		if (RunEnabled && CastEnabled)
		{
			MovementState = EMovementState::CastRun_state;
		}
		if (RunEnabled && !CastEnabled)
		{
			MovementState = EMovementState::Run_state;
		}
	}
	
	CharacterUpdate();
}

void ATDSCharacter::CharacterJump()
{
	if (bIsJumped == false)
	{
		bIsJumped = true;
		CurrentHeight = this->GetActorLocation().Z;
		GetWorld()->GetTimerManager().SetTimer(TimerHandle, this, &ATDSCharacter::JumpTick, 0.1f, true);
		AddMovementInput(FVector(0.0f, 0.0f, 1.0f), AxisZ);
	}
}

void ATDSCharacter::JumpTick()
{
	PreviousHeight = CurrentHeight;
	CurrentHeight = this->GetActorLocation().Z;
	if (CurrentHeight == PreviousHeight)
	{
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle); 
		bIsJumped = false;
	}
}

void ATDSCharacter::HorizontalCameraRotation(float Side)
{
	FRotator Rotator = CameraBoom->GetRelativeRotation();
	if (Side == 1.0f)
	{
		Rotator.Yaw += 2.5;
		CameraBoom->SetRelativeRotation(Rotator);
	}
	else if (Side == -1.0f)
	{
		Rotator.Yaw -= 2.5;
		CameraBoom->SetRelativeRotation(Rotator);
	}
}

void ATDSCharacter::SpawnMagicProjectile()
{
	FVector StartLocation = this->GetActorLocation();
	FVector EndLocation;
	FVector Direction;

	StartLocation.Z = this->GetActorLocation().Z + 50.0f;

	Direction = this->GetActorForwardVector();

	AMagicProjectile* Projectile = GetWorld()->SpawnActor<AMagicProjectile>(ProjectileClass, StartLocation, Direction.Rotation());
	
	if (Projectile && Projectile->ProjectileMovementComponent)
	{
		Projectile->PawnPointer = this;
		FHitResult HitResult;
		APlayerController* PC = Cast<APlayerController>(GetController());
		PC->GetHitResultUnderCursor(ECC_Visibility, true, HitResult);
		EndLocation = HitResult.Location;

		//ProjectileSpeed = FVector::Dist(StartLocation, EndLocation);

		Projectile->ProjectileMovementComponent->Activate();

		Projectile->ProjectileMovementComponent->Velocity = Direction * ProjectileSpeed;
	}
}
