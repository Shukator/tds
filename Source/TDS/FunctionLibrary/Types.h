// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Types.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	Walk_state UMETA(DisplayName = "Walk state"),
	CastWalk_state UMETA(DisplayName = "Cast Walkd state"),
	Run_state UMETA(DisplayName = "Run state"),
	CastRun_state UMETA(DisplayName = "Cast Run state"),
	
	//For animations
	Jump_state UMETA(DisplayName = "Falling state")

};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float WalkSpeed = 600.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float CastWalkSpeed = 300.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float RunSpeed = 800.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float CastRunSpeed = 500.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float JumpSpeed = 500.0f;
};

USTRUCT(BlueprintType)
struct FElementType
{
	GENERATED_BODY()

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Element")
		FString FireElement = "Fire";
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Element")
		FString IceElement = "Ice";
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Element")
		FString EarthElement = "Earth";
};

UCLASS()
class TDS_API UTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	

};